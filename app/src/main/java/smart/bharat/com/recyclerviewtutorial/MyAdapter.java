package smart.bharat.com.recyclerviewtutorial;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;

/**
 * Created by Bharat on 3/16/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {


List<StudentRecord> records= Collections.emptyList();

  public MyAdapter(List<StudentRecord> records) {
    this.records = records;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item,parent,false);
    return  new ViewHolder(v);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {

    StudentRecord ob=records.get(position);

    if(ob!=null){
      holder.name.setText(ob.getName());
      holder.rollno.setText(String.valueOf(ob.getRollno()));
      holder.email.setText(ob.getEmailId());
    }

  }

  @Override public int getItemCount() {
    return records!=null?records.size():0;
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    TextView name,rollno,email;

    public ViewHolder(View itemView) {
      super(itemView);
      name= (TextView) itemView.findViewById(R.id.tv_name);
      rollno= (TextView) itemView.findViewById(R.id.tv_roll);
      email= (TextView) itemView.findViewById(R.id.tv_email);

    }
  }


}
