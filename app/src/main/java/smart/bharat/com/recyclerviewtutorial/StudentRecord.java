package smart.bharat.com.recyclerviewtutorial;

/**
 * Created by Bharat on 3/17/2017.
 */

public class StudentRecord {

  String name;
  int rollno;
  String emailId;

  public StudentRecord(String name, int rollno, String emailId) {
    this.name = name;
    this.rollno = rollno;
    this.emailId = emailId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRollno() {
    return rollno;
  }

  public void setRollno(int rollno) {
    this.rollno = rollno;
  }

  public String getEmailId() {
    return emailId;
  }

  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }
}
