package smart.bharat.com.recyclerviewtutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


  private RecyclerView recyclerView;
  private MyAdapter adapter;
  private List<StudentRecord> records;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);


    //initialize recyclerview
    recyclerView= (RecyclerView) findViewById(R.id.rv);

    /**
     *  this line tells the recyclerview  that every item added or removed will be of same height.
     *  If you dont set this it will check if the size of the item has changed and thats expensive
     */
    recyclerView.setHasFixedSize(true);


    //set layout manager
    recyclerView.setLayoutManager(new LinearLayoutManager(this));


    //initialize list
    records=new ArrayList<>();

    //insert some student records to the list
     populateListWithRecords();

    //initialize adapter and pass records list
    adapter=new MyAdapter(records);

    //finally attach adapter to the recyclerview
    recyclerView.setAdapter(adapter);


  }

  private void populateListWithRecords() {

  records.add(new StudentRecord("bharat",141113014,"bharat@gmail.com"));
  records.add(new StudentRecord("raj",141113015,"raj@gmail.com"));
  records.add(new StudentRecord("deepak",141113016,"deepak@gmail.com"));
  records.add(new StudentRecord("Arjun",141113017,"arjun@gmail.com"));
  records.add(new StudentRecord("Patel",141113018,"patel@gmail.com"));
  records.add(new StudentRecord("Afzal",141113019,"afzal@gmail.com"));
  records.add(new StudentRecord("Mark",141113020,"mark@gmail.com"));
  records.add(new StudentRecord("Steven",141113021,"steven@gmail.com"));
  records.add(new StudentRecord("Bhola",141113022,"xyz@gmail.com"));
  records.add(new StudentRecord("Dinesh",141113023,"xyz@gmail.com"));
  records.add(new StudentRecord("Mary",141113024,"mary@gmail.com"));
  records.add(new StudentRecord("Shahrukh",141113025,"srk@gmail.com"));

  }
}
